(function ($, Drupal) {
  Drupal.behaviors.wtMailjetFullscreen = {
    attach: function (context, settings) {
      $('#wt_mailjet_fullscreen_button', context).once('wtMailjetFullscreen').on('click', function() {
        console.info('1');
        $('body').toggleClass('wt_mailjet_body_hide_overflow');
        $('#wt_mailjet_iframe_container').toggleClass('force_fullscreen');
        $('#wt_mailjet_iframe_icon').toggleClass('fa-expand-arrows-alt fa-compress-arrows-alt');
      });
    }
  };
}(jQuery, Drupal));
