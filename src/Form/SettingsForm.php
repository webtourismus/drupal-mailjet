<?php /** @noinspection PhpComposerExtensionStubsInspection */

namespace Drupal\wt_mailjet\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SettingsForm extends ConfigFormBase {

  const API_URL = 'https://api.mailjet.com/v3/REST/';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'wt_mailjet.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wt_mailjet_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('wt_mailjet.settings');
    $form['api'] = [
      '#type' => 'fieldset',
    ];

    $form['api']['welcome'] = [
      '#markup' => $this->t('Please enter your Mailjet API Key and Secret Key which can be found in <a href="https://app.mailjet.com/account/api_keys">your Mailjet account.</a>'),
    ];

    $form['api']['api_key'] = [
      '#type' => 'textfield',
      '#title' => t('API Key'),
      '#default_value' => $config->get('api_key'),
      '#required' => TRUE,
    ];

    $form['api']['secret_key'] = [
      '#type' => 'textfield',
      '#title' => t('Secret Key'),
      '#default_value' => $config->get('secret_key'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $response = $this->callMailjet($form_state->getValue('api_key'), $form_state->getValue('secret_key'), 'myprofile');

    if (!$response) {
      $this->messenger()->addError($this->t('Could not connect to your Mailjet profile. Please verify that you have entered your API and secret key correctly.'));
      $form_state->setErrorByName('api_key');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $apiKey = $form_state->getValue('api_key');
    $secretKey = $form_state->getValue('secret_key');

    $config = $this->configFactory()->getEditable('wt_mailjet.settings');
    $config->set('api_key', $apiKey);
    $config->set('secret_key', $secretKey);
    $config->save();

    // first let's check if we can reuse an existing api key
    $response = $this->callMailjet($apiKey, $secretKey, 'apitoken');
    $json = json_decode($response);
    if (!$json || !($json->Count > 0)) {
      // we did not find an existing api key, let's create a new one
      $response = $this->callMailjet($apiKey, $secretKey, 'apitoken', [CURLOPT_POST => TRUE, CURLOPT_POSTFIELDS => ['APIKeyALT' => $apiKey, 'TokenType' => 'iframe', 'IsActive' => TRUE]]);
      $json = json_decode($response);
    }

    if ($json && $json->Count > 0) {
      $config->set('token', $json->Data[0]->Token);
      $config->save();
      $this->messenger()->addMessage($this->t('Credentials successfully saved.'));
    }
    else {
      $this->messenger()->addError($this->t('Credentials were saved, but token was NOT generated! Please try again.'));
    }
  }

  protected function callMailjet($apiKey, $secretKey, $apiEndpoint, $curlOptions = NULL) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, self::API_URL . $apiEndpoint);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_USERPWD, $apiKey . ':' . $secretKey);
    if (!empty($curlOptions)) {
      curl_setopt_array($curl, $curlOptions);
    }
    $result = curl_exec($curl);
    curl_close($curl);
    return $result;
  }

}
