<?php

namespace Drupal\wt_mailjet\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

class IframeController extends ControllerBase {

  const IFRAME_DOMAIN = 'https://app.mailjet.com';
  const DEFAULT_LANGUAGE = 'en';
  const AVAILABLE_LANGUAGES = [
    'en' => 'en_US',
    'fr' => 'fr_FR',
    'de' => 'de_DE',
    'es' => 'es_ES',
    'it' => 'it_IT',
  ];

  protected function getUrl($basepath) {
    return self::IFRAME_DOMAIN . "/{$basepath}?t={$this->getToken()}&locale={$this->getLanguage()}&show_menu=none";
  }

  protected function getLanguage() {
    $lang = $this->languageManager()->getCurrentLanguage()->getId();
    return self::AVAILABLE_LANGUAGES[$lang] ?? self::AVAILABLE_LANGUAGES[self::DEFAULT_LANGUAGE];
  }

  protected function getToken() {
    return $this->config('wt_mailjet.settings')->get('token');
  }

  public function showIframe($basepath) {
    $build = [];

    if (!$this->getToken()) {
      $build = [
        '#type' => '#markup',
        '#markup' => $this->t('<p>API token not found. Please check API Key and Secret Key in the <a href="@mailjet-settings-form">Mailjet Settings</a>.<p>', ['@mailjet-settings-form' => Url::fromRoute('wt_mailjet.settings')->toString()]),
      ];
      return $build;
    }

    $build = [
      '#type' => 'inline_template',
      '#template' => '<div id="wt_mailjet_iframe_container">
          <button id="wt_mailjet_fullscreen_button" class="button"><i id="wt_mailjet_fullscreen_icon" class="far fa-expand-arrows-alt"></i> {{ "Toggle Fullscreen"|t }}</button>
          <iframe src="{{ url }}" id="wt_mailjet_iframe"></iframe>
        </div>',
      '#context' => [
        'url' => $this->getUrl($basepath),
      ],
      '#attached' => [
        'library' => ['wt_mailjet/admin']
      ]
    ];
    return $build;
  }
}
